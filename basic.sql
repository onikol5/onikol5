-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 15 2015 г., 16:00
-- Версия сервера: 5.5.45
-- Версия PHP: 5.4.44

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `basic`
--

-- --------------------------------------------------------

--
-- Структура таблицы `assign`
--

CREATE TABLE IF NOT EXISTS `assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=141 ;

--
-- Дамп данных таблицы `assign`
--

INSERT INTO `assign` (`id`, `posts_id`, `mark`, `data`) VALUES
(61, 7, 280, '2015-12-09'),
(62, 8, 151, '2015-12-09'),
(63, 9, 266, '2015-12-09'),
(64, 10, 174, '2015-12-09'),
(65, 11, 137, '2015-12-09'),
(66, 12, 274, '2015-12-09'),
(67, 13, 97, '2015-12-09'),
(68, 14, 3, '2015-12-09'),
(69, 15, 80, '2015-12-09'),
(70, 16, 184, '2015-12-09'),
(71, 17, 272, '2015-12-09'),
(72, 18, 124, '2015-12-09'),
(73, 19, 138, '2015-12-09'),
(74, 20, 249, '2015-12-09'),
(75, 21, 144, '2015-12-09'),
(76, 22, 225, '2015-12-09'),
(77, 23, 188, '2015-12-09'),
(78, 24, 83, '2015-12-09'),
(79, 25, 116, '2015-12-09'),
(80, 26, 146, '2015-12-09'),
(81, 7, 67, '2015-12-11'),
(82, 8, 124, '2015-12-11'),
(83, 9, 161, '2015-12-11'),
(84, 10, 110, '2015-12-11'),
(85, 11, 238, '2015-12-11'),
(86, 12, 38, '2015-12-11'),
(87, 13, 179, '2015-12-11'),
(88, 14, 209, '2015-12-11'),
(89, 15, 155, '2015-12-11'),
(90, 16, 198, '2015-12-11'),
(91, 17, 257, '2015-12-11'),
(92, 18, 286, '2015-12-11'),
(93, 19, 110, '2015-12-11'),
(94, 20, 181, '2015-12-11'),
(95, 21, 9, '2015-12-11'),
(96, 22, 144, '2015-12-11'),
(97, 23, 242, '2015-12-11'),
(98, 24, 15, '2015-12-11'),
(99, 25, 105, '2015-12-11'),
(100, 26, 37, '2015-12-11'),
(101, 7, 74, '2015-12-12'),
(102, 8, 298, '2015-12-12'),
(103, 9, 32, '2015-12-12'),
(104, 10, 255, '2015-12-12'),
(105, 11, 251, '2015-12-12'),
(106, 12, 63, '2015-12-12'),
(107, 13, 32, '2015-12-12'),
(108, 14, 178, '2015-12-12'),
(109, 15, 213, '2015-12-12'),
(110, 16, 218, '2015-12-12'),
(111, 17, 76, '2015-12-12'),
(112, 18, 92, '2015-12-12'),
(113, 19, 289, '2015-12-12'),
(114, 20, 226, '2015-12-12'),
(115, 21, 117, '2015-12-12'),
(116, 22, 287, '2015-12-12'),
(117, 23, 57, '2015-12-12'),
(118, 24, 4, '2015-12-12'),
(119, 25, 259, '2015-12-12'),
(120, 26, 2, '2015-12-12'),
(121, 7, 19, '2015-12-13'),
(122, 8, 70, '2015-12-13'),
(123, 9, 288, '2015-12-13'),
(124, 10, 30, '2015-12-13'),
(125, 11, 170, '2015-12-13'),
(126, 12, 90, '2015-12-13'),
(127, 13, 41, '2015-12-13'),
(128, 14, 194, '2015-12-13'),
(129, 15, 154, '2015-12-13'),
(130, 16, 207, '2015-12-13'),
(131, 17, 262, '2015-12-13'),
(132, 18, 132, '2015-12-13'),
(133, 19, 149, '2015-12-13'),
(134, 20, 38, '2015-12-13'),
(135, 21, 151, '2015-12-13'),
(136, 22, 285, '2015-12-13'),
(137, 23, 183, '2015-12-13'),
(138, 24, 248, '2015-12-13'),
(139, 25, 263, '2015-12-13'),
(140, 26, 43, '2015-12-13');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1449468587),
('m150211_165754_create_user_table', 1449468589),
('m150211_170614_create_post_table', 1449468589),
('m150211_193807_drop_post_table', 1449468590),
('m150330_153014_drop_user_table', 1449468590),
('m150330_153138_create_user_table', 1449468590),
('m150527_004551_drop_user_table', 1449468590),
('m150527_004737_create_user_table', 1449468590),
('m150711_083606_create_profile_table', 1449468590),
('m150804_021125_add_secret_key_in_user_table', 1449468591);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `text` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `text`, `date`) VALUES
(1, 3, 'Text 1111111', '2015-12-14 06:23:21'),
(6, 3, 'Some1', '2015-12-14 06:23:24'),
(7, 4, 'Some 2', '2015-12-14 15:05:24'),
(8, 4, 'Some 3', '2015-12-13 18:55:50'),
(9, 4, 'Some 4', '2015-12-13 18:56:08'),
(10, 4, 'Some 5', '2015-12-13 18:56:08'),
(11, 4, 'Some 6', '2015-12-13 18:56:23'),
(12, 4, 'Some 7_1', '2015-12-14 07:48:25'),
(13, 4, 'Some 8', '2015-12-13 18:56:40'),
(14, 4, 'Some 9', '2015-12-13 18:56:40'),
(15, 4, 'Some 10', '2015-12-13 18:56:54'),
(16, 4, 'Some 11', '2015-12-13 18:56:54'),
(17, 4, 'Some 12', '2015-12-13 18:57:08'),
(18, 4, 'Some 13', '2015-12-13 18:57:08'),
(19, 4, 'Some 14', '2015-12-13 18:57:22'),
(20, 4, 'Some 15', '2015-12-13 18:57:22'),
(21, 4, 'Some 16', '2015-12-13 18:57:35'),
(22, 4, 'Some 17', '2015-12-13 18:57:35'),
(23, 4, 'Some 18', '2015-12-13 18:57:53'),
(24, 4, 'Some 19', '2015-12-13 18:57:53'),
(25, 4, 'Some 19', '2015-12-13 18:58:07'),
(26, 4, 'Some 21', '2015-12-13 18:58:07');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `status`, `auth_key`, `created_at`, `updated_at`) VALUES
(3, 'Oleg', 'some@mail.com', '$2y$13$D9nInIDw2ggT29Wqf8OGKug6hbViepH.ILckE7ekPdqjnX6I2PCUy', 10, 'OUP3Py5c3b8vnZH3YNI53knQ_45XVgRS', 1449482256, 1449482256),
(4, 'Oleg1', 'some1@mail.com', '$2y$13$5gcc24TdaBYIbZS3rMKGYuGP7Ixat2YPFDWHoeR9V8I2voduXQ1ka', 10, 'pG2Hjy9EXpKP_1J516cNYrUjOYiuPL5L', 1449483640, 1449484239);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
