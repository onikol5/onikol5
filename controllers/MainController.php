<?php
namespace app\controllers;

use Yii;
use app\models\RegForm;
use app\models\LoginForm;
use app\models\User;
use app\models\Posts;
use app\models\PostsSearch;
use app\models\Assign;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\db\Query;


class MainController extends \yii\web\Controller
{
	public $layout = 'basic';
	public $defaultAction = 'index';
	
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
			'access' => [
				'class' => AccessControl::ClassName(),
				'only' => ['create', 'update', 'delete'],
				'rules' => [
					[
						'actions' => ['create', 'update', 'delete'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
        ];
    }
	
    public function actionIndex()
    {
		if (!Yii::$app->user->isGuest):
		$AllPosts = (new Query())
			->select(['*'])
			->from('posts')
			->where(['user_id' => Yii::$app->user->getId()]);
		else:
			$AllPosts = Posts::find();
		endif;
		
		if ($AllPosts)
		{
			$pages = new Pagination(['totalCount' => $AllPosts->count(), 'pageSize'=>20]);			
			$posts = $AllPosts->offset($pages->offset)->limit($pages->limit)->all();
			
			return $this->render('index', ['posts' => $posts, 'pages' => $pages]);
		}
    }
	
	public function actionReg(){
		$model = new RegForm();
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()):
			if ($user = $model->reg()):
				if ($user->status === User::STATUS_ACTIVE):
					if (Yii::$app->getUser()->login($user)):
						return $this->goHome();
					endif;
				endif;
			else:
				Yii::$app->session->setFlash('error', 'Возникла ошибка при регистрации');
				Yii::error('Ошибка при регистрации');
				return $this->refresh();
			endif;			
		endif;
		
		return $this->render(
			'reg',
			['model' => $model]
		);
	}
	
	public function actionLogin(){
		$model = new LoginForm();
				
		if ($model->load(Yii::$app->request->post()) && $model->login()):
			return $this->goBack();
		endif;
		
		return $this->render(
			'login',
			['model' => $model]
		);
	}
	
	public function actionLogout(){
		Yii::$app->user->logout();
		return $this->redirect(['/main/index']);	
	}
	
	public function actionSearch()
    {
        $search1 = Yii::$app->request->post('search');

		$search = (new \yii\db\Query())
			->select(['*'])
			->from('posts')
			->where(['like', 'text', $search1]);
		
		if (!Yii::$app->user->isGuest):
			$search->andWhere(['user_id' => Yii::$app->user->getId()]);
		endif;
		
		if ($search)
		{
			$pages = new Pagination(['totalCount' => $search->count(), 'pageSize'=>20]);			
			$posts = $search->offset($pages->offset)->limit($pages->limit)->all();
			
			return $this->render(
            'search',
            [
				'title' => $search1,
				'pages' => $pages,
                'posts' => $posts
            ]);
		}      
    }
	
	/**
     * Displays a single Posts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Posts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Posts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);//(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Posts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
	
	public function actionAssigning()
    {
		$search1 = Yii::$app->request->post('data');
		
        if ($search1 != "dd-mm-yy" and $search1 != "") {
			$prom = explode("-", $search1);
			$search_new = $prom[2]."-".$prom[1]."-".$prom[0];
			$AllPosts = (new Query())
				->select(['id'])
				->from('posts')
				->where(['user_id' => Yii::$app->user->getId()])
				->all();
			
			foreach ($AllPosts as $arr)
				Yii::$app->db->createCommand("INSERT INTO `assign` (`id`,`posts_id`, `mark`, `data`) VALUES ('',".$arr['id'].",".rand(0, 300).",'".$search_new."')")->execute();
			return $this->redirect(['index']);
        } else {
			$name = "";
			$search = (new Query())
				->select(['*'])
				->from('user')
				->where(['id' => Yii::$app->user->getId()])
				->all();
			foreach($search as $arr1)
				$name = $arr1['username'];
			
            return $this->render('assigning', [
				'name' => $name
            ]);
        }
    }
	
	public function actionStat($id)
    {	
		$model = Posts::findOne($id);		
        return $this->render('stat',['name' => $model['text'], 'id' => $id]);
	}

	public function actionStatact()
    {
		$search1 = Yii::$app->request->post('data1');
		$search2 = Yii::$app->request->post('data2');
		$id = Yii::$app->request->post('id');
		$model = Posts::findOne($id);
		
        if (($search1 != "dd-mm-yy" and $search1 != "") and ($search2 != "dd-mm-yy" and $search2 != "")) {
			$prom = explode("-",$search1);
			$search1_1 = $prom[2]."-".$prom[1]."-".$prom[0];
			$prom = explode("-",$search2);
			$search2_1 = $prom[2]."-".$prom[1]."-".$prom[0];
			
			$search = (new Query())
				->select(['*'])
				->from('assign')
				->where(['posts_id' => $id])
				->andWhere(['BETWEEN','data',$search1_1,$search2_1])
				->all();
			
			return $this->render('stat',['name' => $model['text'], 'id' => $id, 'search' => $search]);
		}
		else  return $this->render('stat',['name' => $model['text'], 'id' => $id]);
	}
	

    /**
     * Finds the Posts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Posts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findAssign($id)
    {
        if (($model = Assign::findAll("posts_id=".$id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
