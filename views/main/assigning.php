<?
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<p>Слова пользователя <?= $name ?></p>
<div class="posts-form">

    <?php ActiveForm::begin([
		'action' => ['/main/assigning'],
		'method' => 'post',
		'options' => ['class' => 'navbar-form navbar-right']
	]);
	echo Html::input(
		'type:text',
		'data',
		'',
		[
			'class' => 'form-control',
			'value' => 'dd-mm-yy',
			'onfocus' => 'this.select();lcs(this)',
			'onclick' => 'event.cancelBubble=true;this.select();lcs(this)'
		]
	);
	echo "<div class='form-group'>";
	echo Html::SubmitButton(
		'Назначить',
		['class' => 'btn btn-success']
	);
	echo "</div>";
	ActiveForm::end(); ?>

</div>