<?
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<p>Сатистика по слову <?= $name ?></p>
<div class="posts-form">

    <?php ActiveForm::begin([
		'action' => ['/main/statact'],
		'method' => 'post',
		'options' => ['class' => 'navbar-form navbar-right']
	]);
	
	echo Html::input(
		'hidden',
		'id',
		$id,
		'',
		[
			'value' => $id
		]
	);
	
	echo "C ". Html::input(
		'type:text',
		'data1',
		'',
		[
			'class' => 'form-control',
			'value' => 'dd-mm-yy',
			'onfocus' => 'this.select();lcs(this)',
			'onclick' => 'event.cancelBubble=true;this.select();lcs(this)'
		]
	);
	echo "По ". Html::input(
		'type:text',
		'data2',
		'',
		[
			'class' => 'form-control',
			'value' => 'dd-mm-yy',
			'onfocus' => 'this.select();lcs(this)',
			'onclick' => 'event.cancelBubble=true;this.select();lcs(this)'
		]
	);
	echo "<div class='form-group'>";
	echo Html::SubmitButton(
		'Посмотреть',
		['class' => 'btn btn-success']
	);
	echo "</div>";	
	ActiveForm::end();
	
	if ($search)
	{
		echo "<div class='panel panel-default'>
		<table class='table'>";
		$count = 0;
		$count_prom = 0;
		$i = 0;
		foreach($search as $arr)
		{
			if ($i == 0)	$count = 0;
			else	$count = $arr['mark'] - $count_prom;
			$color = "red";
			if ($count>=0) $color="green";		
			echo "<tr><td>".$arr['mark']."</td><td>".$arr['data']."</td><td style='color:".$color."'>".$count."</td></tr>";
			$count_prom = $arr['mark'];
			$i++;
		}
		echo "</table>
		</div>";
	}
	?>	
</div>